package com.example.dmastruz.newmemory;

public class PictureItem {

    private String qrCodeValue;
    private int picture;

    public PictureItem() {
    }

    public PictureItem(String qrCodeValue, int picture) {
        this.qrCodeValue = qrCodeValue;
        this.picture = picture;
    }

    //This constructor is used whenever a new PictureItem is created (after adding a new PictureItem)
    public PictureItem(int picture){
        this.picture = picture;
    }

    public String getQrCodeValue() {
        return qrCodeValue;
    }

    public void setQrCodeValue(String qrCodeValue) {
        this.qrCodeValue = qrCodeValue;
    }

    public int getPicture() {
        return picture;
    }

    public void setPicture(int picture) {
        this.picture = picture;
    }
}
