package com.example.dmastruz.newmemory;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

//creates view from data --> "https://www.youtube.com/watch?v=uic3TVp_j3M"
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private List<PictureItem> pictureItems;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView qrCodeValue;
        public ImageView picture;
        public Button button;

        public MyViewHolder(View view) {
            super(view);
            qrCodeValue = (TextView) view.findViewById(R.id.qrCodeCalue);
            picture = (ImageView) view.findViewById(R.id.picture);
            button = (Button) view.findViewById(R.id.add_button);
        }
    }

    public RecyclerViewAdapter(Context mContext, List<PictureItem> pictureItems) {
        this.mContext = mContext;
        this.pictureItems = pictureItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;

        if(viewType == R.layout.picture_card){
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.picture_card,parent,false);
        }else if(viewType == R.layout.button_card){
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.button_card,parent,false);
        }

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        if(position == pictureItems.size()){
            /* holder.button.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){

                }
            }); */
        }else{
            PictureItem pictureItem = pictureItems.get(position);
            holder.qrCodeValue.setText(pictureItem.getQrCodeValue());

            // loading album cover using Glide library
            Glide.with(mContext).load(pictureItem.getPicture()).into(holder.picture);
        }
    }

    private final int CONTENT_TYPE = 1;
    private final int BUTTON_TYPE = 2;

    @Override
    public int getItemViewType(int position) {
        return (position == pictureItems.size()) ? R.layout.button_card : R.layout.picture_card;
    }

    @Override
    public int getItemCount() {return pictureItems.size() + 1;}

}
